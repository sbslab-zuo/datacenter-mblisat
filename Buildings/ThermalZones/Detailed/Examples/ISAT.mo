within Buildings.ThermalZones.Detailed.Examples;
package ISAT
  "Package that tests the models for coupled simulation between Modelica and ISAT"
  extends Modelica.Icons.ExamplesPackage;

  package UsersGuide
                     //isat users guide to be added
    extends Modelica.Icons.Information;
    annotation (preferredView="info",
    Documentation(info="<html>
<h4>Introduction</h4>
<p>
For an introduction to the coupled simulation and for detailed information
on <a href=\"modelica://Buildings.ThermalZones.Detailed.CFD\">Buildings.ThermalZones.Detailed.CFD</a>,
please refer to
<a href=\"modelica://Buildings.ThermalZones.Detailed.UsersGuide.CFD\">Buildings.ThermalZones.Detailed.UsersGuide.CFD</a>.
For a step by step guide on performing the coupled simulation, please refer
to <a href=\"modelica://Buildings.ThermalZones.Detailed.Examples.FFD.Tutorial\">Buildings.ThermalZones.Detailed.Examples.FFD.Tutorial</a>.
</p>
<h4>Files for the Coupled Simulation</h4>
<p>
The source code of the FFD program is located at <code>Buildings/Resources/src/FastFluidDynamics</code>.
The <code>Buildings</code> library contains precompiled versions of this source
code in the subdirectories of <code>Buildings/Resources/Library</code>.
</p>
<p>
To run the coupled simulation with FFD, the following files are needed and
provided in the <code>Buildings</code> library:
</p>
<ul>
<li>
dynamic link files located at <code>Buildings/Resources/Library</code>:
<ul>
<li>
Windows: <code>ffd.dll</code> and <code>ffd.lib</code>
</li>
<li>
Linux: <code>libffd.so</code>
</li>
</ul>
</li>
<li>
FFD input files for simulation parameters, called <code>*.ffd</code> and
located at <code>Buildings/Resources/Data/ThermalZones/Detailed/Examples/FFD/</code>
</li>
<li>
Mesh files <code>*.cfd</code>  and obstacles files <code>*.dat</code>
generated by the program <code>SCI_FFD</code> and located
in the same folder as the FFD input files (<code>Buildings/Resources/Data/ThermalZones/Detailed/Examples/FFD/</code>).
<ul>
<li>The <code>SCI_FFD</code> program is maintained
at <a href=\"https://github.com/FastFluidDynamics/Mesh\">https://github.com/FastFluidDynamics/Mesh</a>.
</li>
<li>
The Mesh files <code>*.cfd</code> and obstacles files <code>*.dat</code> must
be located in the same folder as the FFD input files.
</li>
</ul>
</li>
</ul>
<h4>Compiling library files</h4>
<p>
The FFD program can be compiled into a dlls using Microsoft Visual Studio
Express in Windows and gcc in Linux. Compiled files are distributed
with the <code>Buildings</code> library.
If you want to compile the files yourself, proceed as follows:
<ul>
<li>
On Windows, method 1:
<ol>
<li>
Go to <code>Buildings/Resources/src/FastFluidDynamics/</code>
</li>
<li>
Run <code>compile.bat</code> as administrator
</li>
</ol>
</li>
<li>
On Windows, method 2:
<ol>
<li>
Open cmd.
</li>
<li>
Change to the directory <code>Buildings/Resources/src/FastFluidDynamics/</code>
</li>
<li>
type <code>compile.bat</code>
</li>
</ol>
</li>
<li>
On Linux:
<ol>
<li>
Open a console.
</li>
<li>
Change to the directory <code>Buildings/Resources/src/FastFluidDynamics/</code>
</li>
<li>
Type <code>make all</code>
</li>
</ol>
</li>
</ul>
</html>",   revisions="<html>
<ul>
<li>
September 07, 2017, by Thierry Nouidui:<br/>
Refactored the FFD C-code and revised the documentation.
This is for
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/612\">issue 612</a>.
</li>
</ul>
</html>"));
  end UsersGuide;

  model ForcedConvection "Ventilation with forced convection in an empty room"
    extends Modelica.Icons.Example;
    // to be added
    // isat example 1: room ventilation application
    // isat example 2: data center application

  end ForcedConvection;

  model DataCenter "A data center room with IT racks"
    extends Modelica.Icons.Example;
    // to be added
    // isat example 1: room ventilation application
    // isat example 2: data center application

  end DataCenter;

  package BaseClasses "Package with base classes for Buildings.ThermalZones.Detailed.Examples.FFD"
    extends Modelica.Icons.BasesPackage;

    partial model PartialRoom "Partial model for a room"
      package MediumA = Buildings.Media.Air (
            T_default=283.15) "Medium model";
      parameter Integer nConExtWin=0 "Number of constructions with a window";
      parameter Integer nConBou=0
        "Number of surface that are connected to constructions that are modeled inside the room";
      parameter Integer nSurBou=0
        "Number of surface that are connected to the room air volume";
      parameter Integer nConExt=0
        "Number of exterior constructions withour a window";
      parameter Integer nConPar=0 "Number of partition constructions";
      Buildings.ThermalZones.Detailed.CFD roo(
        redeclare package Medium = MediumA,
        nConBou=nConBou,
        nSurBou=nSurBou,
        nConExt=nConExt,
        sensorName={"Occupied zone air temperature","Velocity"},
        useCFD=true,
        nConPar=nConPar,
        nConExtWin=nConExtWin,
        AFlo=1*1,
        hRoo=1,
        linearizeRadiation=true,
        samplePeriod=60,
        cfdFilNam="modelica://Buildings/Resources/Data/ThermalZones/Detailed/Examples/FFD/OnlyWall.ffd",
        massDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
        lat=0.00022318989969804) "Room model"
        annotation (Placement(transformation(extent={{46,20},{86,60}})));
      Modelica.Blocks.Sources.Constant qConGai_flow(k=0) "Convective heat gain"
        annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
      Modelica.Blocks.Sources.Constant qRadGai_flow(k=0) "Radiative heat gain"
        annotation (Placement(transformation(extent={{-60,80},{-40,100}})));
      Modelica.Blocks.Routing.Multiplex3 multiplex3_1
        annotation (Placement(transformation(extent={{-20,40},{0,60}})));
      Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
        annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
      Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
        filNam=Modelica.Utilities.Files.loadResource("modelica://Buildings/Resources/weatherdata/USA_IL_Chicago-OHare.Intl.AP.725300_TMY3.mos"), TDryBul=
            293.15)
        annotation (Placement(transformation(extent={{160,140},{180,160}})));
    equation
      connect(qRadGai_flow.y, multiplex3_1.u1[1]) annotation (Line(
          points={{-39,90},{-32,90},{-32,57},{-22,57}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(qConGai_flow.y, multiplex3_1.u2[1]) annotation (Line(
          points={{-39,50},{-22,50}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(qLatGai_flow.y, multiplex3_1.u3[1]) annotation (Line(
          points={{-39,10},{-32,10},{-32,43},{-22,43}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(multiplex3_1.y, roo.qGai_flow) annotation (Line(
          points={{1,50},{20,50},{20,48},{44.4,48}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(weaDat.weaBus, roo.weaBus) annotation (Line(
          points={{180,150},{190,150},{190,57.9},{83.9,57.9}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None));
      annotation (
        Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
                200,160}}), graphics),
        Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{200,
                160}})),
        Documentation(info="<html>
<p>
The partial model describes a room with only interior walls.
</p>
</html>",     revisions="<html>
<ul>
<li>
August 13, 2013, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"));
    end PartialRoom;
  annotation (preferredView="info", Documentation(info="<html>
<p>
This package contains base classes that are used to construct the models in
<a href=\"modelica://Buildings.ThermalZones.Detailed.Examples.FFD\">Buildings.ThermalZones.Detailed.Examples.FFD</a>.
</p>
</html>"));
  end BaseClasses;
annotation (Documentation(info="<html>
<p>
This package tests the coupled simulation of the model
<a href=\"Buildings.ThermalZones.Detailed.CFD\">Buildings.ThermalZones.Detailed.CFD</a> with the Fast Fluid Dynamics (FFD) program.
Different cases with various boundary conditions are evaluated.
The models in this package do not represent realistic buildings, but
are rather designed to test the coupled simulation.
</p>
</html>", revisions="<html>
<ul>
<li>
January 21, 2014, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"));
end ISAT;
